<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\StoresMock;
use App\Http\Controllers\StoreController;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MapTest extends TestCase
{

	/**
     * PHP Unit Setup Function
     */
	public function setUp(){
		$this->Stores = new StoreController();
		$this->StoresDbMock = new StoresMock();
	}

	/**
     * Get Formated Records
     * Retrieves store records, formats them and returns the list. 
     * 
     * @author Jason Marchalonis
     * @param array $records The DB store records
     * @return array $storeList The mereged and formated records 
     */
	public function getFormatedRecords( $records = [], $recursive = false ){
		$list1 = $this->Stores->extractStoreNames( $records  );
    	$list2 = $this->Stores->extractStoreLocations( $records );
    	$storeList = $this->Stores->mergeStoreLists( $list1, $list2 );

    	if( $recursive == true ){
    		return [ 'stores' => $storeList, 'names' => $list1, 'locations' => $list2 ];
    	}

    	return $storeList;
	}

    /**
     * Test abilty to merge seperated store DB records. 
     * Once we extract the store names and locations, we have to add them back after geo-encoding
     * 
	 * @test
	 */
    public function storeMergeList(){
    	$storeList = $this->getFormatedRecords( $this->StoresDbMock->getActiveStoreList(), true );

    	$this->assertEquals( $this->StoresDbMock->convertListToArray( $storeList['stores'] ), $this->Stores->mergeStoreLists( $storeList['names'], $storeList['locations'] ) );
    }

    /**
     * Test to get a properly formated infowindow string
     * Once we extract the store names and locations, we have format the info for each map maker
     * 
     * * @test
     */
    public function getStoreFormatedInfoWindowString (){
    	$storeList = $this->getFormatedRecords( $this->StoresDbMock->getActiveStoreList() );
    	$info = 'Apple Lincoln Road<br />1021 Lincoln Rd<br />Miami Beach, FL 33139<br /><a href="tel:(305) 421-0400">(305) 421-0400</a>';

    	$this->assertEquals( $info , $this->Stores->formatMapInfoMarker( $storeList[1] ) );
    }

    /**
     * Test to check ability to extract a store's ge-coordinates 
     * Once we extract the store names and locations, we have to add them back after geo-encoding
     * 
     * * @test
     */
    public function getStoreFormatedLatLongString(){
    	$storeList = $this->getFormatedRecords( $this->StoresDbMock->getActiveStoreList() );

    	$this->assertEquals( '25.957297, -80.145069' , $this->Stores->extractLatLonMarker( $storeList[3] ) );
    }

    /**
	 * Using the DB mocked data, do we have the same record count reflected.
	 * Since we have 5 records, the could sould be the same. 
	 * 
	 * @test
	 */
    public function dbStoreCount(){
    	$this->assertCount( 5 , $this->Stores->extractStoreNames( $this->StoresDbMock->getActiveStoreList() ));
    }




}
