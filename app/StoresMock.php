<?php

namespace App;

use \App\Stores;
use Illuminate\Database\Eloquent\Model;

class StoresMock extends Stores
{
    protected $table = 'stores';
    
    /**
     * Get Active Store List Mocked
     * Used to find and return the active stores in the database
     * 
     * @author Jason Marchalonis
     * @return array $records The actve store records
     */
    public function getActiveStoreList(){
    	$mockedRecords = [
    		(object) [ 
    			'name' => 'Apple Brickell City Centre', 'id' => 2, 'street_number' => 701, 
    			'street' => 'S Miami Ave', 'city' => 'Miami', 'state' => 'FL', 'zip_code' => '33130', 
    			'telephone' => '(786) 843-4350', 'lat' => '25.767164', 'long' => '-80.193081',
    		],
    		(object) [ 
    			'name' => 'Apple Lincoln Road', 'id' => 3, 'street_number' => 1021, 
    			'street' => 'Lincoln Rd', 'city' => 'Miami Beach', 'state' => 'FL', 'zip_code' => '33139', 
    			'telephone' => '(305) 421-0400', 'lat' => '25.790706', 'long' => '-80.139375',
    		],
    		(object) [ 
    			'name' => 'Apple Dadeland', 'id' => 4, 'street_number' => 7535, 
    			'street' => 'Dadeland Mall Cir W', 'city' => 'Kendall', 'state' => 'FL', 'zip_code' => '33156', 
    			'telephone' => '(305) 341-9812', 'lat' => '25.690057', 'long' => '-80.313705',
    		],
    		(object) [ 
    			'name' => 'Apple Aventura', 'id' => 5, 'street_number' => 701, 
    			'street' => 'Biscayne Blvd', 'city' => 'Aventura', 'state' => 'FL', 'zip_code' => '33180', 
    			'telephone' => '(305) 914-9826', 'lat' => '25.957297', 'long' => '-80.145069',
    		],
    		(object) [ 
    			'name' => 'Apple The Falls', 'id' => 6, 'street_number' => 8888, 
    			'street' => 'SW 136th St', 'city' => 'Miami', 'state' => 'FL', 'zip_code' => '33176', 
    			'telephone' => '(305) 234-4565', 'lat' => '25.644382', 'long' => '-80.339479',
    		],
    	];
    	
    	return $mockedRecords;
    }

    public function convertListToArray( $list = [] ){

    	$mockedRecordsArray = [];
    	foreach ($list as $key => $item) {
    		$mockedRecordsArray[] = (array) $item;
    	}
    	
    	return $mockedRecordsArray;
    }

}	
