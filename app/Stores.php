<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
    protected $table = 'stores';

    /**
     * Get Active Store List
     * Used to find and return the active stores in the database
     * 
     * @author Jason Marchalonis
     * @return array $records The actve store records
     */
    public function getActiveStoreList(){
    	$records = $this->where('active', 1)->get();
    	
    	return $records;
    }

}	
