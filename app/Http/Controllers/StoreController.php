<?php

namespace App\Http\Controllers;

use App\Stores;
use App\Http\Controllers\Controller;
use FarhanWazir\GoogleMaps\GMaps;

class StoreController extends Controller
{

    /**
     * Extract Store Names
     * Used to extract and return a list of stores passed. 
     * 
     * @author Jason Marchalonis
     * @param  array/object  $list The list of Database Records
     * @return array $storeList The list of formated store names
     */
    public function extractStoreNames( $list = [] ){
        $storeList = [];
        foreach ($list as $record) {
            $storeList[] = [
                'id' => $record->id,
                'name' => $record->name,
            ];
         }

         return $storeList;
    }

    /**
     * Extract Store Locations
     * Used to extract the store locations for geo-encoding.
     * 
     * @author Jason Marchalonis
     * @param  array/object  $list The list of database Records
     * @return array $locationList The list of formated store locations
     */
    public function extractStoreLocations( $list = [] ){
        $locationList = [];
        foreach ($list as $record) {

            $locationList[] = [
                'street_number' => $record->street_number,
                'street' => $record->street,
                'city' => $record->city,
                'state' => $record->state,
                'zip_code' => $record->zip_code,
                'lat' => $record->lat,
                'long' => $record->long,
                'telephone' => $record->telephone,
            ];
         }

         return $locationList;
    }

     /**
     * Merge Store Lists
     * Used to merge the store lists data 
     * 
     * @author Jason Marchalonis
     * @param  array/object  $list The list of store names
     * @param  array/object  $list2 The list of store locations
     * @return array  The list of formated store data to be plotted
     */
    public function mergeStoreLists( $list = [], $list2 = [] ){
        $i=0;
        $newArray = array();
        foreach($list as $item) {
            $newArray[] = array_merge( $item, $list2[$i] );
            $i++;
        }

        return $newArray;
    }

    /**
     * Format Map Info Marker
     * Used to prepare and format a marker's information window
     * 
     * @author Jason Marchalonis
     * @param  array  $item The store data
     * @return string $infoWindowInfo the formated string of information to display
     */
    public function formatMapInfoMarker( $item = null ){

        $infoWindowInfo = $item['name'] . '<br />';
        $infoWindowInfo .= $item['street_number'] . ' ' . $item['street'] . '<br />';
        $infoWindowInfo .= $item['city'] . ', ' . $item['state'] . ' ' . $item['zip_code'] . '<br />';
        $infoWindowInfo .= '<a href="tel:'.$item['telephone'].'">'.$item['telephone'].'</a>';

        return $infoWindowInfo;
    }

    /**
     * Extract Lat/Long Markert
     * Used to extract the latitude and longatude from a store record
     * 
     * @author Jason Marchalonis
     * @param  array  $item The store data
     * @return string The formated lat/long of the store. 
     */
    public function extractLatLonMarker( $item = null ){
        if( empty($item) ){
            throw new Exception("Error Processing Request", 1);
        }
        return $item['lat'] . ', ' . $item['long'];
    }

     /**
     * Index
     * Returns the index view for the store controller.
     * 
     * @author Jason Marchalonis
     * @return view Stores The store view
     */
    public function index(){

        $this->Stores = new Stores();
        $records = $this->Stores->getActiveStoreList();

        $storeList = $this->extractStoreNames( $records );
        $storeLocations = $this->extractStoreLocations( $records );
        $storeListMerged = $this->mergeStoreLists( $storeList, $storeLocations);

        $this->gmap = new GMaps();

        $config = array();
        $config['map_height'] = "100%";
        $config['zoom'] = "11";
        $config['center'] = 'Miami, Florida';
        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
        }
        centreGot = false;';
        $this->gmap->initialize($config);
        
        // Setup the dynamic markers based
        foreach ($storeListMerged as $key => $store) {
            $marker = array();
            $marker['draggable'] = false;
            $marker['position'] = $this->extractLatLonMarker( $store );
            $marker['infowindow_content'] =  $this->formatMapInfoMarker( $store );  

            $this->gmap->add_marker($marker);
        }

        $map = $this->gmap->create_map();
        
        return view("stores", compact('storeList', 'storeLocations', 'storeListMerged', 'map' ));

    }

}